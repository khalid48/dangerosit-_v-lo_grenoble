import csv
import os
#lire le fichier accidents-velos.csv
def lire_fichier():
    with open('Donnees/accidents-velos.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                print(f'Column  sont  {", ".join(row)}')
                line_count += 1
            else:
                print(f'\t{row[0]}   {row[1]}  {row[2]}.')
                line_count += 1
    print(f'Processed {line_count} lines.')
lire_fichier()


FichierDeTraitement = "Donnees/accidents-velos38.csv" # Fichier intermédiaire
CodeDepartement = "38"
""" supprime le csv s'il existe"""
def csv_Verif():
    if os.path.exists(FichierDeTraitement):
        os.remove(FichierDeTraitement)
        print("Un nouveau fichier source est en cours de traitement et remplacera le précédant")
        csv_Extraction()
    else:
        print("Un fichier source est en cours de traitement")
        csv_Extraction()
"""crée un nouveau csv avec seulement Grenoble et les colonnes désirées"""
def csv_Extraction():
    with open(f"Donnees/accidents-velos.csv", newline='')as csvfiler:
        lecture = csv.reader(csvfiler, delimiter=',')
        with open(FichierDeTraitement, 'a') as csvfilew:
            ecriture = csv.writer(csvfilew, delimiter=",")
            for row in lecture:
                if  row[5] == CodeDepartement:
                    cequonveut= row[0], row[1], row[4], row[5], row[6], row[7], row[8], row[9], row[13], row[25], row[26], row[27], row[32], row[33], row[34], row[36], row[37] 
                    ecriture.writerow(cequonveut)
    print("Un fichier source de data a été traité avec succès")

csv_Extraction()
csv_Verif()
           
