import psycopg2
from psycopg2 import sql
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
from psycopg2.extras import  NamedTupleCursor




def create_connection():
    """ Connect to the PostgreSQL database server """
    conn = None
    try:
        # connect to the PostgreSQL server
        print('Connecting to the PostgreSQL database...')
        conn = psycopg2.connect(host="localhost",
                                dbname="velo_db",
                                user="turbine_velo",
                                password="paswd",
                                port=5432)


        conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)

    except Exception as inst:
        print("I am unable to connect to the database")
        print(inst)

    return conn
 
def create_tables(conn) -> None:
   
    requete = """CREATE TABLE IF NOT EXISTS accidents_velo_38 (
          identifiant_accident bigint,
          date date,
          heure varchar (250) ,
          departement varchar (250) , 
          commune varchar (250),
          lat varchar (250),
          lon varchar (250),
          en_agglomeration varchar(100),
          conditions_atmosperiques varchar(250),
          gravite_accident varchar(250),
          sexe  varchar(250),
          age varchar(250),
          obstacle_mobile_heurte varchar(250),
          localisition_choc varchar(250),
          manoeuvre_avant_accident varchar(250),  
          type_autres_vehicules varchar(250),
          manoeuvre_autres_vehicules varchar (250));
          """
    query_create_select(conn, requete)
     
    requete = """CREATE TABLE IF NOT EXISTS points_noirs(
            
            id  int,
            lat  float,
            lon float);
            """
    query_create_select(conn, requete)

    requete = """CREATE TABLE IF NOT EXISTS code_insee_metropleGrenoble(
             
          code_insee varchar(50)  ) ;
            """
    
    query_create_select(conn, requete)

def query_create_select(conn, requete):
    cur = conn.cursor()
    #print(requete)
    cur.execute(requete)
    return cur



if __name__ == "__main__":
    with create_connection() as conn:
        try:
            create_tables(conn)
        except Exception as e:
            print(e)
        finally:
            conn.commit()