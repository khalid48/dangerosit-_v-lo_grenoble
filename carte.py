from flask import Flask
import folium
import database
import pandas as pd
import json
from folium.plugins import MarkerCluster
import os
app = Flask(__name__)
@app.route('/')
def index():
    conn = database.create_connection()
    cur = conn.cursor()
    map_gr = folium.Map(location=[45.1875602, 5.7357819], tiles="OpenStreetMap", zoom_start=13)
    folium.raster_layers.TileLayer('Stamen Toner').add_to(map_gr)
    folium.raster_layers.TileLayer('Stamen Watercolor').add_to(map_gr)
    folium.raster_layers.TileLayer('CartoDB Positron').add_to(map_gr)
    folium.raster_layers.TileLayer('CartoDB Dark_Matter').add_to(map_gr)
    ### piste cyclable
    lgd_txt = '<span style="color: {col};">{txt}</span>'
    group04_pists = folium.FeatureGroup(name=lgd_txt.format(txt="Pistes cyclables ", col="black"))
    pistcyclable = os.path.join('Donnees/velo.geojson')
    folium.GeoJson(pistcyclable, name='pist').add_to(group04_pists)
    group04_pists.add_to(map_gr)
    ##accident velo
    lgd_txt = '<span style="color: {col};">{txt}</span>'
    group01_accidents = folium.FeatureGroup(name=lgd_txt.format(txt="Accidents_Métropole_Grenoble ", col="black"))
    #cur = database.query_create_select(conn, "select * from  accidents_velo_38 where(lat != '0' AND lon!='0') ;") 
    cur = database.query_create_select (conn,"select *  from accidents_velo_38  join code_insee_metropleGrenoble on code_insee_metropleGrenoble.code_insee=accidents_velo_38.commune  ;")  
    for row in cur:
            date=row[1]
            heure=row[2]
            lat = row[5]
            lon = row[6]
            cond_atmos=row[8]
            grav_accident=row[9]
            sexe=row[10]
            age=row[11]
            color=''
            if grav_accident =="0 - Indemne":
                 color='pink'
            elif grav_accident== "1 - Blessé léger":
                color='orange'
            elif grav_accident== "2 - Blessé hospitalisé" :
            
                color="red"
            elif grav_accident== "3 - Tué":
                color="purple"
            if lat !='0' and lon!='0':
                folium.CircleMarker(
                [lat, lon],
                radius=5,
                color=color,
                fill=True,
                popup=f"Date:{date}  Heure: {heure}h </br>méteo: {cond_atmos}</br> gravite accident:{grav_accident} </br> sexe: {sexe} </br> age: {age} </br>  ",
                fill_color='darkred',
                fill_opacity=0.2
            ).add_to(group01_accidents)
    group01_accidents.add_to(map_gr)
        ##points noirs
    lgd_txt = '<span style="color: {col};">{txt}</span>'
    group02_points = folium.FeatureGroup(name=lgd_txt.format(txt="Points_noirs ", col="black"))
    mc = MarkerCluster()
    #cur= database.query_create_select(conn, "SELECT * FROM  points_noirs WHERE (point_distances >= '0.00' AND point_distances <= '0.8163181674732839') ;")
    cur= database.query_create_select(conn, "SELECT * FROM  points_noirs ;")
    for ligne in cur:
            id_points=(ligne[0])
            latitude=(ligne[1])
            longitude=(ligne[2])

            Nom = str(id_points) + ": " + str(latitude) + " , " + str(longitude)
            if latitude!='' and longitude!='' :
                mc.add_child(folium.Marker(location=[latitude,  longitude],
                popup=Nom,type ="collision",radius=2,
                color="black",opacity=0.3,
            fill=True,))
    group02_points.add_child(mc)
    group02_points.add_to(map_gr)
    folium.LayerControl(collapsed=True).add_to(map_gr)
    map_gr.save("templates/velo.html")
    return map_gr._repr_html_()
if __name__ == '__main__':
            #app.run(port=5000)
            app.run(debug=True)





